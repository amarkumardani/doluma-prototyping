# -*- coding: utf-8 -*-
"""
Created on Wed Apr  5 10:03:44 2017

@author: harsha.va
"""

# Read JSON file and extract the key value pairs
import json
with open("json_data3.txt") as data_file:
    data = json.load(data_file)
msg = data[0]
identity = msg["templateID"]

from neo4j.v1 import GraphDatabase, basic_auth
driver = GraphDatabase.driver("bolt://localhost:7687", auth=basic_auth("neo4j", "amar310884"))
session = driver.session()

session.run("CREATE (n:INST{templateID:1,paytype:2,timestamp:1,threadID:54}) return n")
session.run("CREATE (n:INST{templateID:1,paytype:2,timestamp:1,threadID:55}) return n")

flag_matched = 0

btd_res = session.run("MATCH (a:BTD) return a")
for record in btd_res:
    dict_temp = dict(record)
    dict_node = dict(dict_temp['a'])
    node_template_id = dict_node['templateID']
    
    if node_template_id == identity:
        flag_matched = 1
        cmd_str = "CREATE (n:INST{templateID:" + str(identity) #forming create command

        for key in dict_node.keys():

            if 'p_' in key:
                val = dict_node[key]
                print (val)
                print (type(val))
                if (val in msg.keys()):
                    if type(msg[val]) is str:
                        var_val = "\""+ msg[val] + "\""
                    else:
                        var_val = str(msg[val])
                    print (var_val)
                    cmd_str = cmd_str + "," + val + ":" + var_val  #forming create command
                    
                    exec(key + " = " + var_val )
                    exec('tgt_' + key + '=' + key) # create target variable for expression validation
                
        for key in dict_node.keys():
            if 'v_' in key:
                val = dict_node[key]
                print(val)
                exec(val)     

        cmd_str = cmd_str + "}) return n,ID(n)" #forming create command
        print(cmd_str)
        obj = session.run(cmd_str) #create node
        for recd in obj:
            dict_t1 = dict(recd)
            node_id2 = dict_t1["ID(n)"] # getting the target node ID
        
        
        ### Validating relationship expressions
        rel_fetch = session.run("MATCH (m)-[r]->(n{templateID:{id}}) return m,r,type(r)",id=identity)
        for record in rel_fetch:
            dict_temp = dict(record)
            dict_node = dict(dict_temp['m']) # preceding node connected to the message node from BTD
            dict_rel_t = dict(dict_temp['r']) # target becomes the node that is matched and validated above
            rel_type = dict_temp['type(r)']
            #dict_node_prop = dict(dict_node['a'])
            dict_node_prop = dict_node
            #dict_rel = dict(dict_rel_t['a'])
            dict_rel = dict_rel_t

            inst_nodes = session.run("MATCH (n:INST{templateID:{id}}) return n,ID(n)",id=dict_node_prop['templateID'])
            
            for inst_record in inst_nodes:
                dict_temp1 = dict(inst_record)
                #dict_inst_node = dict(dict(dict_temp1['n'])['a'])
                dict_inst_node = dict(dict_temp1['n'])
                node_id1 = dict_temp1['ID(n)']

                for sr_prop in dict_node_prop.keys():
                    if 'p_' in sr_prop:
                        sr_prop_val = dict_inst_node[dict_node_prop[sr_prop]]
                        if type(sr_prop_val) is str:
                            sr_var_val = "\""+ sr_prop_val + "\""
                        else:
                            sr_var_val = str(sr_prop_val)
                        exec('src_' + sr_prop + '=' + sr_var_val)
                
                valid_value = 0
                for exp in dict_rel.values():
                    exec('if ' + exp + ': valid_value = valid_value+1')
                    print (valid_value)
                    
                if(valid_value == len(dict_rel.values())):
                    print ("matched")
                    rel_cmd_str = "MATCH (m) WHERE ID(m)={id1} MATCH (n) WHERE ID(n)={id2} CREATE (m)-[r:"+ rel_type +"]->(n)"
                    session.run(rel_cmd_str,id1 = node_id1,id2 = node_id2)
                    break

        if flag_matched == 0:
            print ("Message does not match with any node of the BTD")    

session.close()
