# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 17:28:06 2017

@author: harsha.va
"""
### Importing message Json object
import json
with open("json_data.txt") as j_file:
    j_data = json.load(j_file)
msg_dict = j_data[0]
tmpl_ID = msg_dict["ImtID"]

from neo4j.v1 import GraphDatabase, basic_auth
driver = GraphDatabase.driver("bolt://localhost:7687", auth = basic_auth('neo4j','neo4j1'))
session = driver.session()

b_node = session.run("MATCH (n{BTDId:{id}}) RETURN n",id = int(tmpl_ID))
for record in b_node:
    btdn_dict = dict(dict(record)["n"])

prop_ls = btdn_dict['nodeprops'].split(',')
flgv1 = 0
flgv2 = 0
for key in btdn_dict.keys():
    if 'val_' in key:
        flgv1 = flgv1 + 1
        cmd_str = btdn_dict[key]
        for prp in prop_ls:
            # making string or int into format to eliminate execution errors
            if type(msg_dict[prp]) is unicode:
                var_val = "\""+ msg_dict[prp] + "\""
            else:       
                var_val = str(msg_dict[prp])
            cmd_str = cmd_str.replace('$'+prp,var_val)
        print cmd_str
        exec("if " + cmd_str + ': flgv2 = flgv2 + 1')
                
#If the node satisfies all the validations then create a command string and execute it
if flgv1 == flgv2:    
    cmdn_str = "CREATE (n:INST{"
    for key in msg_dict.keys():
        cmdn_str = cmdn_str + key + ":"
        if type(msg_dict[key]) is unicode:
            var_val = "\""+ msg_dict[key] + "\""
        else:       
            var_val = str(msg_dict[key])
        cmdn_str = cmdn_str + var_val + ","
    cmdn_str = cmdn_str.strip(',')
    cmdn_str = cmdn_str + "}) RETURN ID(n)"
    node_created = session.run(cmdn_str)
    print "$$$--- Node Created ---$$$"
for nr in node_created:
    new_node_id = dict(nr)['ID(n)']

###------ Preceeding node realtion -----####
rel_fetch = session.run("MATCH (m)-[r]->(n{BTDId:{id}}) RETURN m,r,type(r)",id=tmpl_ID)
for rec in rel_fetch:
    rec_dict = dict(rec)
    node_dict = dict(rec_dict['m'])
    rel_dict = dict(rec_dict['r'])
    rel_type = rec_dict['type(r)']
    src_prop_ls = node_dict['nodeprops'].split(',')
    
    print node_dict
    print rel_dict
    print rel_type
    print "-----------------------------------"
    
    inst_nodes = session.run("MATCH (n{ImtID:{id}}) RETURN n,ID(n)",id=node_dict['BTDId'])
    for irec in inst_nodes:
        irec_dict = dict(irec)
        inode_dict = dict(irec_dict['n'])
        inode_id = irec_dict['ID(n)']
        flg_rel1 = 0
        flg_rel2 = 0
        for rel_keys in rel_dict.keys():
            if 'val_' in rel_keys:
                flg_rel1 = flg_rel1 + 1
                rval_str = rel_dict[rel_keys]
                for sprp in src_prop_ls: # replacing source variables 
                    # making string or int into format to eliminate execution errors
                    if type(inode_dict[sprp]) is unicode:
                        s_var_val = "\""+ inode_dict[sprp] + "\""
                    else:       
                        s_var_val = str(inode_dict[sprp])
                    rval_str = rval_str.replace('$src.'+sprp,s_var_val)

                for tprp in prop_ls: # replacing target variables
                    # making string or int into format to eliminate execution errors
                    if type(msg_dict[tprp]) is unicode:
                        t_var_val = "\""+ msg_dict[tprp] + "\""
                    else:       
                        t_var_val = str(msg_dict[tprp])
                    rval_str = rval_str.replace('$tgt.'+tprp,t_var_val)
                
                exec("if " + rval_str + ": flg_rel2 = flg_rel2 + 1")
        if flg_rel1==flg_rel2:
            rel_cmd_str = "MATCH (m) WHERE ID(m)={id1} MATCH (n) WHERE ID(n)={id2} CREATE (m)-[r:"+ rel_type +"]->(n)"
            session.run(rel_cmd_str,id1 = inode_id,id2 = new_node_id)
            break
            
###------ Succeeding nodes realtion -----####
rel_fetch = session.run("MATCH (n{BTDId:{id}})-[r]->(m) RETURN m,r,type(r)",id=tmpl_ID)
for rec in rel_fetch:
    rec_dict = dict(rec)
    node_dict = dict(rec_dict['m'])
    rel_dict = dict(rec_dict['r'])
    rel_type = rec_dict['type(r)']
    tgt_prop_ls = node_dict['nodeprops'].split(',')
    
    print node_dict
    print rel_dict
    print rel_type
    print "-----------------------------------"
    
    inst_nodes = session.run("MATCH (n{ImtID:{id}}) RETURN n,ID(n)",id=node_dict['BTDId'])
    for irec in inst_nodes:
        irec_dict = dict(irec)
        inode_dict = dict(irec_dict['n'])
        inode_id = irec_dict['ID(n)']
        flg_rel1 = 0
        flg_rel2 = 0
        for rel_keys in rel_dict.keys():
            if 'val_' in rel_keys:
                flg_rel1 = flg_rel1 + 1
                rval_str = rel_dict[rel_keys]
                for tprp in tgt_prop_ls: # replacing source variables 
                    # making string or int into format to eliminate execution errors
                    if type(inode_dict[tprp]) is unicode:
                        s_var_val = "\""+ inode_dict[tprp] + "\""
                    else:       
                        s_var_val = str(inode_dict[tprp])
                    rval_str = rval_str.replace('$src.'+tprp,s_var_val)

                for sprp in prop_ls: # replacing target variables
                    # making string or int into format to eliminate execution errors
                    if type(msg_dict[sprp]) is unicode:
                        t_var_val = "\""+ msg_dict[sprp] + "\""
                    else:       
                        t_var_val = str(msg_dict[sprp])
                    rval_str = rval_str.replace('$tgt.'+sprp,t_var_val)
                
                exec("if " + rval_str + ": flg_rel2 = flg_rel2 + 1")
        if flg_rel1==flg_rel2:
            rel_cmd_str = "MATCH (m) WHERE ID(m)={id1} MATCH (n) WHERE ID(n)={id2} CREATE (m)-[r:"+ rel_type +"]->(n)"
            session.run(rel_cmd_str,id2 = inode_id,id1 = new_node_id)
            break
#session.close()