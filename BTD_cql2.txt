create(tid1:BTD{BTDId:1,nodeprops:"paytype,timestamp,threadID",val_1:"$paytype in [\"cardA\",\"cardB\",\"cardC\"]"})
create(tid2:BTD{BTDId:2,nodeprops:"paytype,timestamp,threadID,username,pswd",val_1:"$paytype==\"cardA\"",val_2:"$username in [\"harsha.va\",\"amar.d\",\"priam.d\"]",val_3:"$pswd in [\"harsha\",\"amar\",\"priam\"]"})
create(tid3:BTD{BTDId:3,nodeprops:"paytype,timestamp,threadID,bank,username,pswd",val_1:"$paytype==\"cardB\"",val_2:"$username in [\"harsha.va\",\"amar.d\",\"priam.d\"]",val_3:"$pswd in [\"harsha\",\"amar\",\"priam\"]"})
create(tid4:BTD{BTDId:4,nodeprops:"paytype,timestamp,threadID,bank",val_1:"$paytype==\"cardC\"",val_2:"$bank in [\"HDFC\",\"ICICI\",\"SBI\"]"})
create(tid5:BTD{BTDId:5,nodeprops:"timestamp,threadID,bank,cardNo,cvvNo,verifyTx",val_1:"$cvvNo in [123,567, 901]",val_2:"$bank in [\"HDFC\",\"ICICI\",\"SBI\"]"})
create(tid6:BTD{BTDId:6,nodeprops:"timestamp,threadID,otp,loginSuccess",val_1:"$otp in [1234,5678,9012]",val_2:"$loginSuccess in [\"True\", \"False\"]"})
create(tid7:BTD{BTDId:7,nodeprops:"timestamp,threadID,remarks,loginSuccess",val_1:"$remarks==\"otpReceived\"",val_2:"$loginSuccess in [\"True\", \"False\"]"})
create(tid8:BTD{BTDId:8,nodeprops:"timestamp,threadID,cvvNo,spswd,verifyTx",val_1:"$cvvNo in [123,567,901]",val_2:"$spswd in [\"harsha2\",\"amar2\",\"priam2\"]"})
create(tid9:BTD{BTDId:9,nodeprops:"timestamp,threadID,cvvNo,otp,verifyTx,genOTPSuccess",val_1:"$otp in [1234,5678,9012]",val_2:"$genOTPSuccess in [\"True\",\"False\"]"})
create(tid10:BTD{BTDId:10,nodeprops:"timestamp,threadID,paySuccessFlag",val_1:"$paySuccessFlag in [\"True\",\"False\"]"})


CREATE
	(tid1)-[:Goes_to{val_1:"$src.paytype==\"cardA\"",val_2:"$tgt.paytype==\"cardA\"",val_3:"$src.threadID==$tgt.threadID"}]->(tid2),
	(tid1)-[:Goes_to{val_1:"$src.paytype==\"cardB\"",val_2:"$tgt.paytype==\"cardB\"",val_3:"$src.threadID==$tgt.threadID"}]->(tid3),
	(tid1)-[:Goes_to{val_1:"$src.paytype==\"cardC\"",val_2:"$tgt.paytype==\"cardC\"",val_3:"$src.threadID==$tgt.threadID"}]->(tid4),
	(tid2)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid6),
	(tid3)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid7),
	(tid4)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid5),
	(tid5)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID",val_2:"$src.cvvNo==$tgt.cvvNo",val_3:"$src.verifyTx==$tgt.verifyTx"}]->(tid8),
	(tid5)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID",val_2:"$src.cvvNo==$tgt.cvvNo",val_3:"$src.verifyTx=!$tgt.verifyTx"}]->(tid9),
	(tid6)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid10),
	(tid7)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid10),
	(tid8)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid10),
	(tid9)-[:Goes_to{val_1:"$src.threadID==$tgt.threadID"}]->(tid10)


